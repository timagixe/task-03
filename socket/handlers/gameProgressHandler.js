// ALMOST DONE - CHECK BOTTOM
import { texts } from '../../data';
import { getCurrentRoomId, getUserFromRoom, mapOfRooms } from './roomHandler';
import { letsStopGameProcess } from './gameProcessHandler';

let textLength;
export const mapOfResults = new Map();

// RETREIVES ROOM USERS RESULTS FOR ROOM
export const fetchRoomResults = (roomId) => {
    return mapOfResults.get(roomId) ? mapOfResults.get(roomId) : [];
};

// UPDATES MAP OF RESULTS WITH USER RESULT
export const updateResultsWithUser = (roomId, username) => {
    const room = mapOfRooms.get(roomId);
    let currentResults = mapOfResults.get(roomId)
        ? mapOfResults.get(roomId)
        : [];

    room.forEach((user) => {
        if (user.username == username) {
            currentResults.push({ username, progress: user.progress });
        }
    });

    mapOfResults.set(roomId, currentResults);
};

// MERGE USERS WITH NOT COMPLETED AND COMPLETED CHALLANGE
export const mergeResults = (roomId) => {
    const currentRoom = mapOfRooms.get(roomId);
    const completeResults = mapOfResults.get(roomId)
        ? mapOfResults.get(roomId)
        : [];

    let partialResults = [];

    currentRoom.forEach((user) => {
        // MOVE USERS WITH NOT COMPLETED CHALLANGE
        if (user.progress < 100)
            partialResults.push({
                username: user.username,
                progress: user.progress,
            });
    });

    // SORT RESULTS
    partialResults.sort((a, b) => {
        return (a.progress - b.progress) * -1;
    });

    // MERGING
    mapOfResults.set(roomId, completeResults.concat(partialResults));
};

// DROP DATA ABOUT RESULTS
export const dropResults = (roomId) => {
    mapOfResults.set(roomId, []);
};

// MONITORING USERS PROGRESS WHEN GAME IS STARTED
export const monitorUsersProgress = (io, socket) => {
    socket.on('KEY_PRESSED', (completedLength) => {
        const progress = getProgress(textLength, completedLength);
        const roomId = getCurrentRoomId(socket);
        const username = socket.handshake.query.username;
        updateUserProgress(socket, progress);

        if (progress == 100) updateResultsWithUser(roomId, username);

        io.to(roomId).emit('UPDATE_USER_PROGRESS', username, progress);

        if (isGameDone(socket)) {
            letsStopGameProcess(io, socket);
        }
    });
};

// UPDATES TEXTLENGTH
export const setTextLength = (textId) => {
    textLength = texts[textId].length;
    return;
};

// GETS PROGRESS IN PERCENTAGE
const getProgress = (fullLength, completedLength) => {
    return Math.ceil((100 * completedLength) / fullLength);
};

// UPDATES USER PROGRESS
const updateUserProgress = (socket, progress) => {
    const user = getUserFromRoom(socket);
    if (user) {
        user.progress = progress;
    }
};

// CHECKS IF GAME IS DONE
export const isGameDone = (socket) => {
    let gameIsDone = true;
    const roomId = getCurrentRoomId(socket);
    const room = mapOfRooms.get(roomId);
    room.forEach((user) => {
        if (user.progress < 100) {
            gameIsDone = false;
            return;
        }
    });
    return gameIsDone;
};
