// DONE
import {
    SECONDS_TIMER_BEFORE_START_GAME,
    SECONDS_FOR_GAME,
    MAXIMUM_USERS_FOR_ONE_ROOM,
} from '../config';
import { texts } from '../../data';
import { setTextLength } from './gameProgressHandler';
import {
    removeRoomTimers,
    setRoom,
    getUserFromRoom,
    getCurrentRoomId,
    mapOfRooms,
    updateRoomStatus,
    updateRoomWithTimer,
} from './roomHandler.js';
import {
    mergeResults,
    fetchRoomResults,
    dropResults,
} from './gameProgressHandler';

// UPDATE USER STATUS
const updateStatus = (socket) => {
    const user = getUserFromRoom(socket);
    if (user) {
        user.ready = !user.ready;
    }
};

// CHECKS IF GAME IS READY
const isGameReady = (socket) => {
    const roomId = getCurrentRoomId(socket);
    const room = mapOfRooms.get(roomId);
    let gameIsReady = true;
    if (room) {
        room.forEach((user) => {
            if (!user.ready) gameIsReady = false;
        });
        return gameIsReady;
    } else {
        return false;
    }
};

// STARTS THE GAME
const letsStartGame = (io, socket, roomId) => {
    updateRoomStatus(roomId, 'started');
    io.emit('HIDE_ROOM', roomId);
    const textId = Math.floor(Math.random() * texts.length);
    setTextLength(textId);
    io.in(roomId).emit('START_GAME');
    const text = texts[textId];
    io.in(roomId).emit('FETCH_TEXT', text);
    startTimerBeforeGame(io, socket, roomId);
};

// STARTS TIMER BEFORE GAME
const startTimerBeforeGame = (io, socket, roomId) => {
    let seconds = SECONDS_TIMER_BEFORE_START_GAME;
    let timerId = setTimeout(function timeDecrement() {
        io.in(roomId).emit('UPDATE_BEFORE_GAME_TIMER', seconds);
        if (seconds == 0) {
            letsStartGameProcess(io, socket, roomId);
            return;
        }
        seconds -= 1;
        timerId = setTimeout(timeDecrement, 1000);
        updateRoomWithTimer(roomId, timerId);
    }, 1000);
    updateRoomWithTimer(roomId, timerId);
};

// STARTS THE GAME PROCESS
const letsStartGameProcess = (io, socket, roomId) => {
    let seconds = SECONDS_FOR_GAME;
    io.in(roomId).emit('UPDATE_GAME_TIMER', seconds);
    let timerId = setTimeout(function timeDecrement() {
        io.in(roomId).emit('UPDATE_GAME_TIMER', seconds);
        if (seconds == 0) {
            letsStopGameProcess(io, socket);
            return;
        }
        seconds -= 1;
        timerId = setTimeout(timeDecrement, 1000);
        updateRoomWithTimer(roomId, timerId);
    }, 1000);
    updateRoomWithTimer(roomId, timerId);
};

// PROCESSES THE GAME
export const gameProcess = (io, socket) => {
    socket.on('CHANGE_STATUS', () => {
        const roomId = getCurrentRoomId(socket);
        const username = socket.handshake.query.username;
        io.in(roomId).emit('CHANGE_STATUS_SUCCESS', username);
        updateStatus(socket);
        if (isGameReady(socket)) letsStartGame(io, socket, roomId);
    });

    socket.on('IS_READY', (username) => {
        const roomId = getCurrentRoomId(socket);
        const room = mapOfRooms.get(roomId);
        room.forEach((user) => {
            if (user.username == username) {
                socket.emit('CHECK_STATUS', username, user.ready);
                return;
            }
        });
    });
};

// HANDLES STOPPING THE GAME
export const letsStopGameProcess = (io, socket, roomName) => {
    let roomId;
    if (!roomName) {
        roomId = getCurrentRoomId(socket);
    } else {
        roomId = roomName;
    }
    if (roomId) {
        removeRoomTimers(roomId);
        mergeResults(roomId);
        const results = fetchRoomResults(roomId);
        dropResults(roomId);
        io.to(roomId).emit('STOP_GAME', results);
        updateRoomStatus(roomId, 'created');
        setRoom(roomId);
        const room = mapOfRooms.get(roomId);
        room.forEach((user) => {
            io.in(roomId).emit('CHECK_STATUS', user.username, user.ready);
            io.in(roomId).emit('UPDATE_USER_PROGRESS', user.username, 0);
        });
        if (room.size < MAXIMUM_USERS_FOR_ONE_ROOM)
            io.emit('SHOW_ROOM', roomId);
    }
};
