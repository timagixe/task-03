import {
    createRoomOnRequest,
    fetchRoomsOnline,
    enterRoom,
    leaveRoom,
    userLeftTheGame,
    fetchRooms,
} from './handlers/roomHandler';

import {
    gameProcess,
    letsStopGameProcess,
} from './handlers/gameProcessHandler';
import { monitorUsersProgress } from './handlers/gameProgressHandler';
import { mainHandler, disconnectHandler } from './handlers/handlerIndex';

export default (io) => {
    io.on('connection', (socket) => {
        const username = socket.handshake.query.username;

        mainHandler(io, socket);

        // HANDLE DISCONNECT
        socket.on('disconnecting', () => {
            disconnectHandler(io, socket, username);
        });
    });
};
