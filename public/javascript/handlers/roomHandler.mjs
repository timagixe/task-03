import {
    addNewRoom,
    addListenerJoinRoom,
    joinRoom,
} from '../helpers/onRoom.mjs';
import {
    addUserInRoom,
    showGamePage,
    DeleteUserFromRoom,
} from '../helpers/updateUserInRoom.mjs';

export const fetchUsersInRoom = (socket) => {
    socket.on('FETCH_USERS_IN_ROOM', (usernames, roomName) => {
        usernames.forEach((username) => {
            addUserInRoom(username);
            updateStatus(socket, username);
        });
        showGamePage(socket, sessionStorage.username, roomName);
    });
    socket.on('UPDATE_ROOM_WITH_USER', (username) => {
        addUserInRoom(username);
        updateStatus(socket, username);
    });
    socket.on('DELETE_USER_FROM_ROOM', (username) => {
        DeleteUserFromRoom(username);
    });
    socket.on('CHECK_STATUS', (username, status) => {
        const readyStatus = document.getElementById(`${username}-ready`);

        if (status) {
            if (readyStatus.className.indexOf('not-ready') != -1) {
                readyStatus.className = readyStatus.className.replace(
                    'not-ready',
                    'ready'
                );
            } else {
                readyStatus.className = readyStatus.className.concat(' ready');
            }
        } else {
            if (readyStatus.className.indexOf('ready') != -1) {
                readyStatus.className = readyStatus.className.replace(
                    'ready',
                    'not-ready'
                );
            } else {
                readyStatus.className = readyStatus.className.concat(
                    ' not-ready'
                );
            }
        }
    });
};

const updateStatus = (socket, username) => {
    socket.emit('IS_READY', username);
};

export const fetchRooms = (socket) => {
    socket.emit('FETCH_ROOMS');
    socket.on('FETCH_ROOMS_SUCCESS', (rooms) => {
        rooms.forEach((room) => {
            addNewRoom(room);
            socket.emit('FETCH_ROOMS_ONLINE', room);
            addListenerJoinRoom(socket, room);
        });
    });
};

export const createRoom = (socket) => {
    socket.on('CREATE_ROOM_SUCCESS', (roomId) => {
        joinRoom(socket, roomId, sessionStorage.username);
    });

    socket.on('UPDATE_ROOMS_LIST', (roomId) => {
        addNewRoom(roomId);
        addListenerJoinRoom(socket, roomId);
    });

    socket.on('CREATE_ROOM_FAILED', (roomId) => {
        alert(
            `Room with name - ${roomId} is full. Please create a different one`
        );
    });
};

export const fetchRoomsOnline = (socket) => {
    socket.on('FETCH_ROOMS_ONLINE_SUCCESS', (roomId, online) => {
        const responseOnline = document.getElementById(`${roomId}-online`);
        responseOnline.innerText = `Online: ${online}`;
    });
};

export const hideShowRoom = (socket) => {
    socket.on('HIDE_ROOM', (roomId) => {
        const disappearedRoom = document.getElementById(`${roomId}-room-card`);
        disappearedRoom.style.display = 'none';
    });
    socket.on('SHOW_ROOM', (roomId) => {
        const appearedRoom = document.getElementById(`${roomId}-room-card`);
        if (appearedRoom) {
            appearedRoom.style.display = 'flex';
        } else {
            addNewRoom(roomId);
        }
    });
    socket.on('DELETE_ROOM', (roomId) => {
        const destroyedRoom = document.getElementById(`${roomId}-room-card`);
        destroyedRoom.remove();
    });
};
