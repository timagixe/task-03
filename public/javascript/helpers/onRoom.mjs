import { createElement } from './domHelper.mjs';

const roomsBlock = document.getElementById('list-of-rooms');

export const addNewRoom = (roomName) => {
    const room = createElement({
        tagName: 'div',
        className: 'room-card',
        attributes: {
            id: `${roomName}-room-card`,
        },
    });

    const roomTitle = createElement({
        tagName: 'div',
        className: 'room-title',
    });
    roomTitle.innerText = roomName;
    const joinRoomButton = createElement({
        tagName: 'button',
        className: 'button join-button',
        attributes: {
            id: `${roomName}-join`,
        },
    });
    joinRoomButton.innerText = 'Join';
    const roomOnline = createElement({
        tagName: 'div',
        className: 'room-online',
        attributes: {
            id: `${roomName}-online`,
        },
    });

    roomOnline.innerText = 'Online: 1';

    room.append(roomTitle);
    room.append(roomOnline);
    room.append(joinRoomButton);

    roomsBlock.append(room);
};

export const joinRoom = (socket, roomId, username) => {
    socket.emit('ENTER_ROOM', roomId);
};

export const addListenerJoinRoom = (socket, roomId) => {
    const joinButton = document.getElementById(`${roomId}-join`);
    joinButton.addEventListener('click', () => joinRoom(socket, roomId));
};
