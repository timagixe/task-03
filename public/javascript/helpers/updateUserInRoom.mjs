import { createElement } from './domHelper.mjs';

const userBlock = document.getElementById('list-of-users');

export const addUserInRoom = (username) => {
    const userCard = createElement({
        tagName: 'div',
        className: 'user-card',
        attributes: {
            id: `${username}-user-card`,
        },
    });

    const usernameBlock = createElement({
        tagName: 'div',
        className: 'username-block',
    });

    const usernameElem = createElement({
        tagName: 'div',
        className: 'username-span',
        attributes: {
            id: `${username}-ready`,
        },
    });
    usernameElem.innerText = username;

    const readyStatus = createElement({
        tagName: 'div',
        className: 'rdy-status',
    });

    const progressBar = createElement({
        tagName: 'div',
        className: 'progress-bar',
    });
    const progress = createElement({
        tagName: 'div',
        className: 'progress',
        attributes: {
            id: `${username}-progress`,
        },
    });

    usernameBlock.append(usernameElem);
    usernameBlock.append(readyStatus);

    progressBar.append(progress);

    userCard.append(usernameBlock);
    userCard.append(progressBar);

    userBlock.append(userCard);
};

export const updateReadyButtonText = (socket, username) => {
    socket.emit('CHANGE_STATUS');
    const readyButton = document.getElementById(`ready-button`);
    if (readyButton.innerText == 'I AM NOT READY') {
        readyButton.innerText = 'I AM READY';
    } else {
        readyButton.innerText = 'I AM NOT READY';
    }
};

export const showGamePage = (socket, username, roomName) => {
    const roomInfo = createRoomInfoElement(socket, roomName);
    const roomPage = document.getElementById('rooms-page');
    const gamePage = document.getElementById('game-page');
    roomPage.style.display = 'none';
    gamePage.style.display = 'flex';
    const gameBlock = document.getElementById('game-block');
    const readyButton = createElement({
        tagName: 'button',
        className: 'ready-button button',
        attributes: {
            id: `ready-button`,
        },
    });
    readyButton.innerText = 'I AM READY';
    readyButton.addEventListener('click', () =>
        updateReadyButtonText(socket, username)
    );
    gameBlock.append(readyButton);

    const userCard = document.getElementById(`${username}-user-card`);
    const newUserCardClassNames = userCard.className.concat(' you');
    userCard.className = newUserCardClassNames;
};

export const DeleteUserFromRoom = (username) => {
    const userCard = document.getElementById(`${username}-user-card`);
    userCard.remove();
};

const createRoomInfoElement = (socket, roomName) => {
    const roomInfo = document.getElementById('room-details');
    const backButton = createElement({
        tagName: 'button',
        className: 'back-button button',
        attributes: {
            id: 'back-button',
        },
    });
    backButton.innerText = 'Back To Rooms';
    const roomNameElem = createElement({
        tagName: 'div',
        className: 'room-name',
    });
    roomNameElem.innerText = `Room: ${roomName}`;
    backButton.addEventListener('click', () => backToLobby(socket));
    roomInfo.append(backButton);
    roomInfo.append(roomNameElem);
    return roomInfo;
};

const backToLobby = (socket) => {
    const roomPage = document.getElementById('rooms-page');
    const gamePage = document.getElementById('game-page');
    const roomInfo = document.getElementById('room-details');
    const usersBlock = document.getElementById('list-of-users');
    const gameBlock = document.getElementById('game-block');
    roomInfo.innerHTML = '';
    gameBlock.innerHTML = '';
    usersBlock.innerHTML = '';
    roomPage.style.display = 'block';
    gamePage.style.display = 'none';
    socket.emit('USER_LEFT_ROOM');
};
